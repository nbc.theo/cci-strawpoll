﻿using AuthnData;
using AuthnData.Classes;
using BC = BCrypt.Net.BCrypt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AuthnData.Repository;
using System.Threading.Tasks;

namespace Authn.Controllers
{
    public class SondageController : Controller
    {
        private readonly ILogger<SondageController> _logger;
        private readonly Function _fonction;
        private readonly IAccountRepository _repoAccounts;
        private readonly ISondageRepository _repoSondages;
        private readonly IAnswerRepository _repoAnswers;
        private readonly IInvitedUserRepository _repoInvitedUsers;
        private readonly IUserAnswerRepository _repoUserAnswer;
        public SondageController(ILogger<SondageController> logger, IAccountRepository accountRepository, ISondageRepository sondageRepository, IAnswerRepository answerRepository, IInvitedUserRepository invitedUserRepository, IUserAnswerRepository userAnswerRepository)
        {
            _logger = logger;
            _fonction = new Function(_logger);
            _repoAccounts = accountRepository;
            _repoSondages = sondageRepository;
            _repoAnswers = answerRepository;
            _repoInvitedUsers = invitedUserRepository;
            _repoUserAnswer = userAnswerRepository;
        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            Accounts account =  await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
            if(account.isConfirmed == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            List<Sondages> sondages = await _repoSondages.GetAllSondagesByAccount(account);
            List<Sondages> allSondages = await _repoSondages.GetAllAsync();
            List<InvitedUsers> invitedUsers = await _repoInvitedUsers.GetAllByEmail(account.Email);
            List<Sondages> invitedSondages = new List<Sondages>();
            foreach (InvitedUsers invited in invitedUsers)
            {
                var sondage = allSondages.FirstOrDefault(c => c.Id == invited.SondageId);
                if(sondage != null)
                {
                    invitedSondages.Add(sondage);
                }
            }
            ViewData["Sondages"] = sondages;
            ViewData["Invitations"] = invitedUsers; // Liste des invitations pour le User
            ViewData["InvtedInSondage"] = invitedSondages; // List des sondages aux quels le user a été invité
            return View();
        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
            if(account == null)
            {
                return View();
            }
            if (account.isConfirmed == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(string question, List<string> answer, DateTime expirationDate, bool isMultipleAnswer, bool expireWithDate, bool notifiedByMail)
        {
            if(answer.Count < 2)
            {
                TempData["Error"] = "Vous devez avoir au moins 2 réponses possibles.";
                return View();
            }
            else
            {
                if(string.IsNullOrEmpty(answer[1]))
                {
                    TempData["Error"] = "Vous devez avoir au moins 2 réponses possibles.";
                    return View();
                }
            }
            if (!string.IsNullOrEmpty(question) || !string.IsNullOrEmpty(answer[0]))
            {
                Sondages newSondage = new Sondages();
                newSondage.Date = DateTime.Now;
                newSondage.Question = question;
                if (isMultipleAnswer)
                {
                    newSondage.isMultipleAnswer = 1;
                }
                else
                {
                    newSondage.isMultipleAnswer = 0;
                }
                if(notifiedByMail)
                {
                    newSondage.notifiedByMail = 1;
                }
                else
                {
                    newSondage.notifiedByMail = 0;
                }
                if (expireWithDate)
                {
                    newSondage.expireWithDate = 1;
                    newSondage.expirationDate = expirationDate;
                }
                else
                {
                    newSondage.expireWithDate = 0;
                    newSondage.expirationDate = expirationDate;
                }
                Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
                if (account == null)
                {
                    return NotFound();
                }
                newSondage.Accounts = account;
                newSondage.viewToken = BC.HashString(newSondage.Id + account.Id + account.Username);
                newSondage.closeToken = BC.HashString(newSondage.viewToken + account.Id + account.Username);
                newSondage.publicToken = BC.HashString(account.Id + account.Username);
                await _repoSondages.CreateNew(newSondage);
                foreach (var answerItem in answer)
                {
                    if (!string.IsNullOrEmpty(answerItem))
                    {
                        Answers answers = new Answers();
                        answers.Date = DateTime.Now;
                        answers.Content = answerItem;
                        answers.Sondages = newSondage;
                        await _repoAnswers.Create(answers);
                    }
                }
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ViewSondage(string token)
        {
            if (User.Identity.IsAuthenticated)
            {
                Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
                if (account.isConfirmed == 0)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    if (!string.IsNullOrEmpty(token))
                    {
                        Sondages sondage = await _repoSondages.GetSondagesByViewToken(token);
                        List<Answers> answersFromSondage = await _repoAnswers.GetAllBySondageId(sondage.Id);
                        if (sondage != null)
                        {
                            if (sondage.isClosed != 1)
                            {
                                if (sondage.expireWithDate == 1)
                                {
                                    if (sondage.expirationDate <= DateTime.UtcNow)
                                    {
                                        sondage.isClosed = 1;
                                        await _repoSondages.UpdateSondage(sondage);
                                        TempData["Error"] = "Le Sondage a expiré vous ne pouvez plus voter";
                                        return View();
                                    }
                                    else
                                    {
                                        if (sondage.Accounts.Id != account.Id)
                                        {
                                            InvitedUsers invited = await _repoInvitedUsers.GetBySondageIdAndEmail(sondage.Id, account.Email);
                                            if (invited != null & sondage.Accounts.Id != account.Id)
                                            {
                                                ViewData["Sondage"] = sondage;
                                                ViewData["Answers"] = answersFromSondage;
                                                UsersAnswers userAnswers = await _repoUserAnswer.GetBySondageIdAndAccountId(sondage.Id, account.Id);
                                                ViewData["userAnswers"] = userAnswers;
                                                return View();
                                            }
                                            else
                                            {
                                                TempData["Error"] = "Vous n'avez pas été invité au Sondage.";
                                                return View();
                                            }
                                        }
                                        else
                                        {
                                            ViewData["Sondage"] = sondage;
                                            ViewData["Answers"] = answersFromSondage;
                                            UsersAnswers userAnswers = await _repoUserAnswer.GetBySondageIdAndAccountId(sondage.Id, account.Id);
                                            ViewData["userAnswers"] = userAnswers;
                                            return View();
                                        }
                                    }
                                }
                                else
                                {
                                    InvitedUsers invited = await _repoInvitedUsers.GetBySondageIdAndEmail(sondage.Id, account.Email);
                                    if (sondage.Accounts.Id != account.Id)
                                    {
                                        if (invited != null)
                                        {
                                            ViewData["Sondage"] = sondage;
                                            ViewData["Answers"] = answersFromSondage;
                                            UsersAnswers userAnswers = await _repoUserAnswer.GetBySondageIdAndAccountId(sondage.Id, account.Id);
                                            ViewData["userAnswers"] = userAnswers;
                                            return View();
                                        }
                                        else
                                        {
                                            TempData["Error"] = "Vous n'avez pas été invité au Sondage.";
                                            return View();
                                        }
                                    }
                                    else
                                    {
                                        ViewData["Sondage"] = sondage;
                                        ViewData["Answers"] = answersFromSondage;
                                        UsersAnswers userAnswers = await _repoUserAnswer.GetBySondageIdAndAccountId(sondage.Id, account.Id);
                                        ViewData["userAnswers"] = userAnswers;
                                        return View();
                                    }
                                }
                            }
                            else
                            {
                                TempData["Error"] = "Le Sondage est fermé, vous avez été redirigé vers la page des résultats";
                                return View("Sondage/ViewResult?token=" + sondage.publicToken);
                            }
                        }
                        else
                        {
                            TempData["Error"] = "Aucun Sondage ne correspond.";
                            return View();
                        }
                    }
                    else
                    {
                        TempData["Error"] = "Aucun Sondage ne correspond.";
                        return View();
                    }
                }
            }
            else
            {
                return Redirect("/login?returnUrl=" + "Sondage/ViewSondage?token="+token);
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ViewSondage(int sondageId, List<string> answerUser)
        {

            Sondages sondage = await _repoSondages.GetSondagesById(sondageId);
            if (sondage != null)
            {
                if (answerUser.Count > 0)
                {
                    UsersAnswers newUserAnswer = new UsersAnswers();
                    Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
                    newUserAnswer = await _repoUserAnswer.GetBySondageIdAndAccountId(sondageId, account.Id);
                    if (account != null)
                    {
                        if (newUserAnswer != null)
                        {
                            newUserAnswer.answerDate = DateTime.Now;
                            newUserAnswer.answers = "";
                            for (int i = 0; i < answerUser.Count; i++)
                            {
                                if (i != (answerUser.Count - 1))
                                {
                                    newUserAnswer.answers += answerUser[i] + ";";
                                }
                                else
                                {
                                    newUserAnswer.answers += answerUser[i];
                                }
                            }
                            await _repoUserAnswer.Update(newUserAnswer);
                            if(sondage.notifiedByMail == 1)
                            {
                                Accounts proprio = await _repoAccounts.GetAccountById(sondage.Accounts.Id);
                                _fonction.SendMailNotifReply(account, proprio, sondage);
                            }
                            TempData["Success"] = "Votre réponse a bien été mis à jour !";
                            return View();
                        }
                        else
                        {
                            newUserAnswer = new UsersAnswers();
                            newUserAnswer.accountId = account.Id;
                            newUserAnswer.sondageId = sondageId;
                            newUserAnswer.answerDate = DateTime.Now;
                            for (int i = 0; i < answerUser.Count; i++)
                            {
                                if (i != (answerUser.Count - 1))
                                {
                                    newUserAnswer.answers += answerUser[i] + ";";
                                }
                                else
                                {
                                    newUserAnswer.answers += answerUser[i];
                                }
                            }
                            if (sondage.notifiedByMail == 1)
                            {
                                Accounts proprio = await _repoAccounts.GetAccountById(sondage.Accounts.Id);
                                _fonction.SendMailNotifReply(account, proprio, sondage);
                            }
                            await _repoUserAnswer.Create(newUserAnswer);
                            TempData["Success"] = "Votre réponse a bien été enregistrée !";
                            return View();
                        }
                    }
                    else
                    {
                        TempData["Error"] = "Votre compte n'a pas été trouvé.";
                        return View();
                    }
                }
                else
                {
                    TempData["Error"] = "Aucune réponse n'a été reçu.";
                    return View();
                }
            }
            else
            {
                TempData["Error"] = "Le Sondage n'a pas été trouvé.";
                return View();
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> CloseSondage(string token)
        {
            Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
            if (account.isConfirmed == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                Sondages sondage = await _repoSondages.GetSondagesByCloseToken(token);
                if (sondage != null)
                {
                    TempData["token"] = token;
                    TempData["sondage"] = sondage;
                    return View();
                }
                return View();
            }

        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CloseSondage(string token, string nothing)
        {
            Sondages sondage = await _repoSondages.GetSondagesByCloseToken(token);
            if (sondage != null)
            {
                sondage.isClosed = 1;
                await _repoSondages.UpdateSondage(sondage);
                TempData["Success"] = "Le Sondage a été fermé.";
                TempData["sondage"] = sondage;
                return View();
            }
            else
            {
                TempData["Error"] = "Le token ne correspond à aucun Sondage.";
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> ViewResult(string token)
        {
            List<Accounts> accounts = await _repoAccounts.GetAllAsync();
            Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
            if (account.isConfirmed == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                Sondages sondage = await _repoSondages.GetSondagesByPublicToken(token);
                List<Answers> answers = await _repoAnswers.GetAllBySondageId(sondage.Id);
                if (sondage != null)
                {
                    List<UsersAnswers> usersAnswers = await _repoUserAnswer.GetAllBySondageId(sondage.Id);
                    int nbTotalVotant = usersAnswers.Count();
                    TempData["accounts"] = accounts;
                    TempData["userAnswers"] = usersAnswers;
                    TempData["sondage"] = sondage;
                    TempData["answers"] = answers;
                    TempData["nbTotalVotant"] = nbTotalVotant.ToString();
                    return View();
                }
                else
                {
                    TempData["Error"] = "Aucun sondage ne correspond";
                    return View();
                }
            }
        }
        [HttpGet]
        public async Task<IActionResult> InviteToSondage(string token)
        {
            Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
            if (account.isConfirmed == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                Sondages sondage = await _repoSondages.GetSondagesByViewToken(token);
                if (account != null & sondage != null)
                {
                    if (account.Id == sondage.Accounts.Id)
                    {
                        TempData["token"] = token;
                        return View();
                    }
                    else
                    {
                        TempData["Error"] = "Vous n'êtes pas le propriétaire de ce sondage";
                        return View();
                    }
                }
                else
                {
                    TempData["Error"] = "Compte ou sondage non trouvé.";
                    return View();
                }
            }
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> InviteToSondage(List<string> emails, string token)
        {
            if (emails != null & emails.Count > 0)
            {
                Sondages sondage = await _repoSondages.GetSondagesByViewToken(token);
                if (sondage != null)
                {
                    foreach (string email in emails)
                    {
                        if (await _repoInvitedUsers.GetBySondageIdAndEmail(sondage.Id, email) == null)
                        {
                            _fonction.SendMailToInvite(email, User.Identity.Name, token);
                            InvitedUsers invitedUser = new InvitedUsers();
                            invitedUser.Mail = email;
                            invitedUser.SondageId = sondage.Id;
                            await _repoInvitedUsers.Create(invitedUser);
                        }
                    }
                    TempData["Success"] = "Invitations envoyées !";
                    return View();
                }
                else
                {
                    TempData["Error"] = "Le Sondage n'a pas été trouvé";
                    return View();
                }
            }
            else
            {
                if(string.IsNullOrEmpty(token))
                {
                    TempData["Error"] = "Aucun mail n'a été envoyé.";
                    return View();
                }
                else
                {
                    return View("InviteToSondage?token=" + token);
                }
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(string token)
        {
            Accounts user = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
            Sondages sondage = await _repoSondages.GetSondagesByViewToken(token);
            if(user != null)
            {
                if(user.Id == sondage.Accounts.Id)
                {
                    ViewData["Sondage"] = sondage;
                    return View();
                }
                else
                {
                    TempData["Error"] = "Le Sondage ne vous appartiens pas.";
                    return View();
                }
            }
            else
            {
                TempData["Error"] = "Aucun utilisateur n'a été trouvé.";
                return View();
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Edit(string token, DateTime expirationDate, bool expireWithDate, bool isMultipleAnswer, bool notifiedByMail, bool isClosed)
        {
            try
            {
                Accounts user = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
                Sondages sondage = await _repoSondages.GetSondagesByViewToken(token);
                if (expireWithDate)
                {
                    sondage.expireWithDate = 1;
                    sondage.expirationDate = expirationDate;
                }
                else
                {
                    sondage.expireWithDate = 0;
                }
                if (isMultipleAnswer)
                {
                    sondage.isMultipleAnswer = 1;
                }
                else
                {
                    sondage.isMultipleAnswer = 0;
                }
                if (notifiedByMail)
                {
                    sondage.notifiedByMail = 1;
                }
                else
                {
                    sondage.notifiedByMail = 0;
                }
                if (isClosed)
                {
                    sondage.isClosed = 1;
                }
                else
                {
                    sondage.isClosed = 0;
                }
                await _repoSondages.UpdateSondage(sondage);
                TempData["Success"] = "Votre sondage a été modifié avec succès !";
                return View();
            }catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                TempData["Error"] = "Une erreur est survenue lors de l'édition du sondage";
                return View();
            }
        }
    }
}
