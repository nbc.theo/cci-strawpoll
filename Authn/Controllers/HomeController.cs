﻿using AuthnData.Models;
using AuthnData;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using AuthnData.Classes;
using Microsoft.AspNetCore.Authorization;
using AuthnData.Repository;
using System.Threading.Tasks;

namespace Authn.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Function _function;
        private readonly IAccountRepository _repoAccounts;

        public HomeController(ILogger<HomeController> logger, IAccountRepository accountRepository)
        {
            _logger = logger;
            _function = new Function(_logger);
            _repoAccounts = accountRepository;
        }

        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                Accounts account = await _repoAccounts.GetAccountByMail(User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault());
                if (account != null)
                {
                    if(account.isConfirmed == 1)
                    {
                        return RedirectToAction("Index", "Sondage");
                    }
                    else
                    {
                        TempData["Error"] = "Votre compte n'est pas activé, activez-le grâce à l'email de validation ou ";
                        TempData["Mail"] = account.Email;
                        return View();
                    }
                }
            }
            return View();
        }
        [Authorize]
        public async Task<IActionResult> Activation(string mail)
        {
            Accounts account = await _repoAccounts.GetAccountByMail(mail);
            if (account != null)
            {
                _function.SendActivationMail(account);
                TempData["Success"] = "Le mail d'activation a bien été renvoyé, vous serez redirigé automatiquement vers la page d'accueil.";
                return View();
            }
            TempData["Error"] = "Aucun compte ne correspond à ce mail";
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [Authorize]
        public async Task<IActionResult> Activate(string token)
        {
            Accounts account = await _repoAccounts.GetAccountsByConfirmationToken(token);
            if( account != null )
            {
                account.isConfirmed = 1;
                await _repoAccounts.UpdateAccountAsync(account);
                TempData["Success"] = "Votre compte a bien été activé !";
                return View();
            }
            else
            {
                TempData["Error"] = "Votre lien ne correspond à aucune activation.";
                return View();
            }
        }
        [HttpGet]
        public IActionResult Privacy()
        {
            return View();
        }
    }
}
