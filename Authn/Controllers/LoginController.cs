﻿using AuthnData;
using AuthnData.Classes;
using BC = BCrypt.Net.BCrypt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AuthnData.Repository;

namespace Authn.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;
        private Function function;
        private readonly IAccountRepository _repo;

        public LoginController(ILogger<LoginController> logger, MyDbContext context, IAccountRepository repo)
        {
            _logger = logger;
            function = new Function(_logger);
            _repo = repo;
        }
        [HttpGet("login")]
        public IActionResult Login(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Validate(string Email, string Password, bool RememberMe , string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var accounts = await _repo.GetAllAsync();
            Accounts accountUser = null;
            foreach (var account in accounts)
            {
                if (account.Email == Email & BC.Verify(Password, account.Password))
                {
                    accountUser = account;
                }
            }
            if (accountUser != null)
            {
                accountUser.LastConnectionDate = DateTime.Now;
                if(RememberMe)
                {
                    accountUser.RememberMe = 1;
                }
                else
                {
                    accountUser.RememberMe = 0;
                }
                await _repo.UpdateAccountAsync(accountUser);
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Email, accountUser.Email));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, accountUser.Username));
                claims.Add(new Claim(ClaimTypes.Name, accountUser.Username));
                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                await HttpContext.SignInAsync(claimsPrincipal);
                if (RememberMe)
                {
                    HttpContext.Items.Add("ExpiresUTC", DateTime.UtcNow.AddMonths(1));
                }
                return Redirect(returnUrl);
            }
            TempData["Error"] = "Error? Username or Password is invalid";
            return View("login");
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Redirect("/");
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Accounts account, string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            try
            {
                if (account == null)
                {
                    TempData["Error"] = "Error? The Account is null";
                    return View("login");
                }
                List<Accounts> accounts = await _repo.GetAllAsync();
                Accounts accountVerif = accounts.Find(c => c.Email == account.Email);
                Accounts accountName = accounts.Find(u => u.Username == account.Username);
                if (accountVerif == null & accountName == null)
                {
                    account.Password = BC.HashPassword(account.Password);
                    account.ResetPasswordToken = function.GetUniqueKey(50);
                    account.ConfirmationToken = BC.HashString(account.Id + account.Username);
                    account.isConfirmed = 0;
                    await _repo.CreateNewAccountAsync(account);
                    function.SendActivationMail(account);
                    return View("login");
                }
                else
                {
                    TempData["Error"] = "Un compte avec une adresse e-mail ou le même nom d'utilisateur similaire a déjà été créé.";
                    return RedirectToAction("Register", "Login");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                TempData["Error"] = ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;
                return View("login");
            }
        }

        [HttpGet]
        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(string email)
        {
            var accounts = await _repo.GetAllAsync();
            Accounts userAcc = null;
            foreach (var account in accounts)
            {
                if (email == account.Email)
                {
                    userAcc = account;
                }
            }
            if (userAcc != null)
            {
                try
                {
                    function.SendMail(userAcc);
                    TempData["Success"] = "Un email de confirmation vous a été envoyé.";
                    return View();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message, ex);
                    TempData["Error"] = "Error? Error by sended Mail";
                    return View();
                }
            }

            TempData["Error"] = "Error? Email is invalid";
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Reset(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                Accounts account = await _repo.GetAccountByResetToken(token);
                if(account != null)
                {
                    ViewData["token"] = token;
                    return View();
                }
            }
            TempData["Error"] = "Token non reconnu";
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Reset(string Password, string token)
        {
            if (!string.IsNullOrEmpty(token) & !string.IsNullOrEmpty(Password))
            {
                Accounts account = await _repo.GetAccountByResetToken(token);
                if (account != null)
                {
                    account.Password = BC.HashPassword(Password);
                    account.LastResetPasswordDate = DateTime.Now;
                    account.ResetPasswordToken = function.GetUniqueKey(50);
                    await _repo.UpdateAccountAsync(account);
                    TempData["Success"] = "Votre mot de passe a été modifié avec succès !";
                    return View();
                }
            }
            TempData["Error"] = "Error? Reset Password error";
            return View();
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> ChangePass()
        {
            Accounts account = await _repo.GetAccountByMail(User.Claims.Where(t => t.Type == ClaimTypes.Email).Select(v => v.Value).FirstOrDefault());
            if(account != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ChangePass(string OldPassword, string Password)
        {
            Accounts account = await _repo.GetAccountByMail(User.Claims.Where(t => t.Type == ClaimTypes.Email).Select(v => v.Value).FirstOrDefault());
            if (account != null)
            {
                if(BC.Verify(OldPassword, account.Password))
                {
                    account.Password = BC.HashPassword(Password);
                    await _repo.UpdateAccountAsync(account);
                    TempData["Success"] = "Votre mot de passe a bien été modifié !";
                    return View();
                }
                else
                {
                    TempData["Error"] = "Erreur le Mot de Passe actuel ne correspond pas.";
                    return View();
                }
            }
            else
            {
                TempData["Error"] = "Erreur lors du chagement du Mot de Passe, vous devez être connecté.";
                return RedirectToAction("Index", "Home");
            }

        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
