﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace Authn.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    LastConnectionDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LastResetPasswordDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ResetPasswordToken = table.Column<string>(type: "text", nullable: true),
                    RememberMe = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    isConfirmed = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    ConfirmationToken = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InvitedUsers",
                columns: table => new
                {
                    Mail = table.Column<string>(type: "varchar(767)", nullable: false),
                    SondageId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvitedUsers", x => new { x.Mail, x.SondageId });
                });

            migrationBuilder.CreateTable(
                name: "UsersAnswer",
                columns: table => new
                {
                    accountId = table.Column<int>(type: "int", nullable: false),
                    sondageId = table.Column<int>(type: "int", nullable: false),
                    answers = table.Column<string>(type: "text", nullable: false),
                    answerDate = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersAnswer", x => new { x.accountId, x.sondageId });
                });

            migrationBuilder.CreateTable(
                name: "Sondages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Question = table.Column<string>(type: "text", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    AccountsId = table.Column<int>(type: "int", nullable: true),
                    isMultipleAnswer = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    viewToken = table.Column<string>(type: "text", nullable: false),
                    closeToken = table.Column<string>(type: "text", nullable: false),
                    publicToken = table.Column<string>(type: "text", nullable: false),
                    expirationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    expireWithDate = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    isClosed = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    notifiedByMail = table.Column<byte>(type: "tinyint unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sondages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sondages_Accounts_AccountsId",
                        column: x => x.AccountsId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(type: "text", nullable: false),
                    SondagesId = table.Column<int>(type: "int", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    ordreNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Answers_Sondages_SondagesId",
                        column: x => x.SondagesId,
                        principalTable: "Sondages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_SondagesId",
                table: "Answers",
                column: "SondagesId");

            migrationBuilder.CreateIndex(
                name: "IX_Sondages_AccountsId",
                table: "Sondages",
                column: "AccountsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Answers");

            migrationBuilder.DropTable(
                name: "InvitedUsers");

            migrationBuilder.DropTable(
                name: "UsersAnswer");

            migrationBuilder.DropTable(
                name: "Sondages");

            migrationBuilder.DropTable(
                name: "Accounts");
        }
    }
}
