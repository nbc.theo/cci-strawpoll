﻿using AuthnData.Classes;
using Microsoft.EntityFrameworkCore;

namespace AuthnData
{
    public class MyDbContext : DbContext
    {

        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {

        }

        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Sondages> Sondages { get; set; }
        public DbSet<Answers> Answers { get; set; }
        public DbSet<UsersAnswers> UsersAnswer { get; set; }
        public DbSet<InvitedUsers> InvitedUsers { get; set; }
       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UsersAnswers>().HasKey(nameof(UsersAnswers.accountId), nameof(UsersAnswers.sondageId));
            modelBuilder.Entity<InvitedUsers>().HasKey(nameof(Classes.InvitedUsers.Mail), nameof(Classes.InvitedUsers.SondageId));
            base.OnModelCreating(modelBuilder);
        }
    }
}
