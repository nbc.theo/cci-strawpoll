﻿using AuthnData.Classes;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData
{
    public class Function
    {
        private readonly ILogger _logger;
        public const string WEB_ADRESS = "https://www.poll-hub.fr/";
        //public const string WEB_ADRESS = "http://localhost:5000/";
        public Function(ILogger logger)
        {
            _logger = logger;
        }
        private static Random random = new Random();
        internal static readonly char[] chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
        public string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray()).Substring(0, 15);
        }

        public string TokenSondage(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray()).Substring(0, 12);
        }

        public string GetUniqueKey(int size)
        {
            byte[] data = new byte[4 * size];
            using (var crypto = RandomNumberGenerator.Create())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            for (int i = 0; i < size; i++)
            {
                var rnd = BitConverter.ToUInt32(data, i * 4);
                var idx = rnd % chars.Length;

                result.Append(chars[idx]);
            }

            return result.ToString();
        }

        public void SendMail(Accounts account)
        {
            string to = account.Email; //To address    
            string from = "no-reply@poll-hub.fr"; //From address    
            MailMessage message = new MailMessage(from, to);
            string mailbody = "Voici votre lien pour réinitialiser votre mot de passe : <a href=\""+ WEB_ADRESS +"Login/Reset?token=" + account.ResetPasswordToken + "\">Cliquez-ici</a>";
            message.Subject = "Réinitialisation du Mot de Passe";
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.ionos.fr", 587); //Gmail smtp
            System.Net.NetworkCredential basicCredential1 = new
            System.Net.NetworkCredential("no-reply@poll-hub.fr", "Websondage20!@");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential1;
            try
            {
              client.Send(message);
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
        }
        public void SendMailNotifReply(Accounts account, Accounts proprio, Sondages sondage)
        {
            string to = proprio.Email; //To address    
            string from = "no-reply@poll-hub.fr"; //From address    
            MailMessage message = new MailMessage(from, to);
            string mailbody = "Bonjour " + proprio.Username + ", <br/> Vous avez une nouvelle réponse à votre sondage  <a href=\"" + WEB_ADRESS + "Sondage/ViewResult?token=" + sondage.publicToken + "\">Cliquez-ici pour voir les résultats</a>";
            message.Subject = "Nouvelle réponse sur : " + sondage.Question;
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.ionos.fr", 587); //Gmail smtp
            System.Net.NetworkCredential basicCredential1 = new
            System.Net.NetworkCredential("no-reply@poll-hub.fr", "Websondage20!@");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential1;
            try
            {
                client.Send(message);
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
        }
        public void SendActivationMail(Accounts account)
        {
            string to = account.Email; //To address    
            string from = "no-reply@poll-hub.fr"; //From address    
            MailMessage message = new MailMessage(from, to);
            string mailbody = "Voici votre lien pour activer votre compte : <a href=\"" + WEB_ADRESS + "Home/Activate?token=" + account.ConfirmationToken + "\">Activer mon compte</a>";
            message.Subject = "Activation du Compte - Poll Hub";
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.ionos.fr", 587); //Gmail smtp
            System.Net.NetworkCredential basicCredential1 = new
            System.Net.NetworkCredential("no-reply@poll-hub.fr", "Websondage20!@");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential1;
            try
            {
                client.Send(message);
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
        }

        public void SendMailToInvite(string mail, string fromUser, string token)
        {
            string to = mail; //To address    
            string from = "no-reply@poll-hub.fr"; //From address    
            MailMessage message = new MailMessage(from, to);
            string mailbody = "Bonjour <br/> Vous avez été inviter pour répondre au sondage de " + fromUser + ", <a href=\"" + WEB_ADRESS + "Sondage/ViewSondage?token=" + token + "\">Cliquez-ici</a> <br/><br/> Si vous n'avez pas encore de compte, nous vous invitions à vous inscrire avant de pouvoir voter sur le Sondage. <a href=\"http://localhost:5000/Login/Register\">S'inscrire</a>";
            message.Subject = "Invitation à un Sondage - PollHub";
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.ionos.fr", 587); //Gmail smtp
            System.Net.NetworkCredential basicCredential1 = new
            System.Net.NetworkCredential("no-reply@poll-hub.fr", "Websondage20!@");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential1;
            try
            {
                client.Send(message);
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
        }
    }
}
