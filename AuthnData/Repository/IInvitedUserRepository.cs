﻿using AuthnData.Classes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public interface IInvitedUserRepository
    {
        Task<List<InvitedUsers>> GetAllAsync();
        Task<List<InvitedUsers>> GetAllBySondageId(int sondageId);
        Task<List<InvitedUsers>> GetAllByEmail(string email);
        Task<InvitedUsers> GetBySondageIdAndEmail(int sondageId, string email);
        Task Create(InvitedUsers invitedUsers);
        Task Update(InvitedUsers invitedUsers);
        Task Delete (InvitedUsers invitedUsers);
    }
}
