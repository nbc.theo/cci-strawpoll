﻿using AuthnData.Classes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private readonly MyDbContext _context;

        public AccountRepository(MyDbContext context)
        {
            _context = context;
        }

        public async Task<List<Accounts>> GetAllAsync()
        {
            var accounts = await _context.Accounts.ToListAsync();
            return accounts;
        }

        public async Task<Accounts> GetAccountById(int id)
        {
            var account = await _context.Accounts.FirstOrDefaultAsync(c => c.Id == id);
            return account;
        }

        public async Task<Accounts> GetAccountByMail(string mail)
        {
            var account = await _context.Accounts.FirstOrDefaultAsync(c => c.Email == mail);
            return account;
        }
        public async Task<Accounts> GetAccountsByConfirmationToken(string confirmationToken)
        {
            if(string.IsNullOrEmpty(confirmationToken))
            {
                throw new ArgumentNullException(nameof(confirmationToken));
            }
            Accounts account = await _context.Accounts.FirstOrDefaultAsync(c => c.ConfirmationToken == confirmationToken);
            return account;
        }
        public async Task UpdateAccountAsync(Accounts account)
        {
            if (account == null | account.Id <= 0)
            {
                throw new ArgumentException(nameof(account));
            }
            _context.Accounts.Update(account);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAccountAsync(Accounts account)
        {
            if (account == null | account.Id <= 0)
            {
                throw new ArgumentException(nameof(account));
            }
            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();
        }

        public async Task CreateNewAccountAsync(Accounts account)
        {
            if (account == null)
            {
                throw new ArgumentException(nameof(account));
            }
            await _context.Accounts.AddAsync(account);
            await _context.SaveChangesAsync();
        }
        public async Task<Accounts> GetAccountByResetToken(string resetToken)
        {
            if(string.IsNullOrEmpty(resetToken))
            {
                throw new ArgumentNullException(nameof(resetToken));
            }
            var account = await _context.Accounts.FirstOrDefaultAsync(c => c.ResetPasswordToken == resetToken);
            return account;
        }
    }
}
