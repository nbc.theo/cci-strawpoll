﻿using AuthnData.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public interface IUserAnswerRepository
    {
        Task<List<UsersAnswers>> GetAllAsync();
        Task<List<UsersAnswers>> GetAllBySondageId(int sondageId);
        Task<List<UsersAnswers>> GetAllByAccountId(int accountId);
        Task<UsersAnswers> GetBySondageIdAndAccountId(int sondageId, int accountId);
        Task Create(UsersAnswers usersAnswers);
        Task Update(UsersAnswers usersAnswers);
        Task Delete(UsersAnswers usersAnswers);
    }
}
