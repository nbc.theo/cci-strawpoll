﻿using AuthnData.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public interface IAccountRepository
    {
        
        Task<List<Accounts>> GetAllAsync();
        Task<Accounts> GetAccountById(int id);
        Task<Accounts> GetAccountByMail(string mail);
        Task<Accounts> GetAccountByResetToken(string resetToken);
        Task<Accounts> GetAccountsByConfirmationToken(string confirmationToken);
        Task UpdateAccountAsync(Accounts account);
        Task DeleteAccountAsync(Accounts account);
        Task CreateNewAccountAsync(Accounts account);
    }
}
