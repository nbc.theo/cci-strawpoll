﻿using AuthnData.Classes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public class InvitedUserRepository : IInvitedUserRepository
    {
        private readonly MyDbContext _context;

        public InvitedUserRepository(MyDbContext context)
        {
            _context = context;
        }

        public async Task<List<InvitedUsers>> GetAllAsync()
        {
            List<InvitedUsers> invitedUsers = await _context.InvitedUsers.ToListAsync();
            return invitedUsers;
        }
        public async Task<List<InvitedUsers>> GetAllBySondageId(int sondageId)
        {
            if(sondageId < 0)
                throw new ArgumentOutOfRangeException(nameof(sondageId));
            List<InvitedUsers> invitedUsers = await _context.InvitedUsers.Where(c => c.SondageId == sondageId).ToListAsync();
            return invitedUsers;
        }
        public async Task<List<InvitedUsers>> GetAllByEmail(string email)
        {
            if(string.IsNullOrEmpty(email))
                throw new ArgumentNullException(nameof(email));
            List<InvitedUsers> invitedUsers = await _context.InvitedUsers.Where(c => c.Mail == email).ToListAsync();
            return invitedUsers;
        }
        public async Task<InvitedUsers> GetBySondageIdAndEmail(int sondageId, string email)
        {
            if (sondageId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sondageId));
            }
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException(nameof(email));
            }
            InvitedUsers invitedUsers = await _context.InvitedUsers.FirstOrDefaultAsync(c => c.SondageId == sondageId & c.Mail == email);
            return invitedUsers;
        }
        public async Task Create(InvitedUsers invitedUsers)
        {
          if(invitedUsers == null)
                throw new ArgumentNullException(nameof(invitedUsers));
          _context.InvitedUsers.Add(invitedUsers);
            await _context.SaveChangesAsync();
        }
        public async Task Update(InvitedUsers invitedUsers)
        {
            if(invitedUsers == null)
                throw new ArgumentNullException(nameof(invitedUsers));
            _context.InvitedUsers.Update(invitedUsers);
            await _context.SaveChangesAsync();
        }
        public async Task Delete(InvitedUsers invitedUsers)
        {
            if(invitedUsers == null)
                throw new ArgumentNullException(nameof(invitedUsers));
            _context.Remove(invitedUsers);
            await _context.SaveChangesAsync();
        }
    }
}
