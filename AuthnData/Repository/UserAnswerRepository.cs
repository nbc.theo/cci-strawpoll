﻿using AuthnData.Classes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public class UserAnswerRepository : IUserAnswerRepository
    {
        private readonly MyDbContext _context;
        
        public UserAnswerRepository(MyDbContext context)
        {
            _context = context;
        }

        public async Task<List<UsersAnswers>> GetAllAsync()
        {
            List<UsersAnswers> usersAnswers = await _context.UsersAnswer.ToListAsync();
            return usersAnswers;
        }
        public async Task<List<UsersAnswers>> GetAllBySondageId(int sondageId)
        {
            if(sondageId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sondageId));
            }
            List<UsersAnswers> usersAnswers = await _context.UsersAnswer
                .Where(c => c.sondageId == sondageId).ToListAsync();
            return usersAnswers;
        }
        public async Task<List<UsersAnswers>> GetAllByAccountId(int accountId)
        {
            if(accountId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(accountId));
            }
            List<UsersAnswers> usersAnswers = await _context.UsersAnswer.Where(c => c.accountId == accountId).ToListAsync();
            return usersAnswers;
        }
        public async Task<UsersAnswers> GetBySondageIdAndAccountId(int sondageId, int accountId)
        {
            if (sondageId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sondageId));
            }
            if (accountId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(accountId));
            }
            UsersAnswers usersAnswers = await _context.UsersAnswer.FirstOrDefaultAsync(c => c.sondageId == sondageId & c.accountId == accountId);
            return usersAnswers;
        }
        public async Task Create(UsersAnswers usersAnswers)
        {
            if(usersAnswers == null)
            {
                throw new ArgumentNullException(nameof(usersAnswers));
            }
            _context.UsersAnswer.Add(usersAnswers);
            await _context.SaveChangesAsync();
        }
        public async Task Update(UsersAnswers usersAnswers)
        {
            if (usersAnswers == null)
            {
                throw new ArgumentNullException(nameof(usersAnswers));
            }
            _context.UsersAnswer.Update(usersAnswers);
            await _context.SaveChangesAsync();
        }
        public async Task Delete(UsersAnswers usersAnswers)
        {
            if (usersAnswers == null)
            {
                throw new ArgumentNullException(nameof(usersAnswers));
            }
            _context.UsersAnswer.Remove(usersAnswers);
            await _context.SaveChangesAsync();
        }
    }
}
