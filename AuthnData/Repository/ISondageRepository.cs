﻿using AuthnData.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public interface ISondageRepository
    {
        Task<List<Sondages>> GetAllAsync();
        Task<Sondages> GetSondagesById(int id);
        Task<List<Sondages>> GetAllSondagesByAccount(Accounts account);
        Task<Sondages> GetSondagesByViewToken(string viewToken);
        Task<Sondages> GetSondagesByCloseToken(string closeToken);
        Task<Sondages> GetSondagesByPublicToken(string publicToken);
        Task CreateNew(Sondages sondage);
        Task DeleteSondage(Sondages sondage);
        Task UpdateSondage(Sondages sondage);
    }
}
