﻿using AuthnData.Classes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public class AnswerRepository : IAnswerRepository
    {
        private readonly MyDbContext _context;

        public AnswerRepository(MyDbContext context)
        {
            _context = context;
        }

        public async Task<List<Answers>> GetAllAsync()
        {
            List<Answers> answers = await _context.Answers
                .Include(b => b.Sondages)
                .ToListAsync();
            return answers;
        }

        public async Task<List<Answers>> GetAllBySondageId(int sondageId)
        {
            if(sondageId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sondageId));
            }
            List<Answers> answers = await _context.Answers
                .Include(b => b.Sondages)
                .Where(c => c.Sondages.Id == sondageId).ToListAsync();
            return answers;
        }
        public async Task Create(Answers answers)
        {
            if(answers == null)
                throw new ArgumentNullException(nameof(answers));
            await _context.Answers.AddAsync(answers);
            await _context.SaveChangesAsync();
        }
        public async Task Update(Answers answers)
        {
            if (answers == null)
                throw new ArgumentNullException(nameof(answers));
            _context.Answers.Update(answers);
            await _context.SaveChangesAsync();
        }
        public async Task Delete(Answers answers)
        {
            if (answers == null)
                throw new ArgumentNullException(nameof(answers));
            _context.Answers.Remove(answers);
            await _context.SaveChangesAsync();
        }
    }
}
