﻿using AuthnData.Classes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public class SondageRepository : ISondageRepository
    {
        private readonly MyDbContext _context;

        public SondageRepository(MyDbContext context)
        {
            _context = context;
        }

        public async Task<List<Sondages>> GetAllAsync()
        {
            List<Sondages> sondages = await _context.Sondages.Include(c => c.Accounts).ToListAsync();
            return sondages;
        }

        public async Task<Sondages> GetSondagesById(int id)
        {
            if(id < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            Sondages sondage = await _context.Sondages.Include(c => c.Accounts).FirstOrDefaultAsync(c => c.Id == id);
            return sondage;
        }
        public async Task<List<Sondages>> GetAllSondagesByAccount(Accounts account)
        {
            if(account == null)
            {
                throw new NullReferenceException(nameof(account));
            }
            List<Sondages> sondages = await _context.Sondages.Include(c => c.Accounts).Where(a => a.Accounts == account).ToListAsync();
            return sondages;
        }

        public async Task<Sondages> GetSondagesByViewToken(string viewToken)
        {
            if(string.IsNullOrEmpty(viewToken))
            {
                throw new ArgumentNullException(nameof(viewToken));
            }
            Sondages sondage = await _context.Sondages.Include(c => c.Accounts).FirstOrDefaultAsync(c => c.viewToken == viewToken);
            return sondage;
        }
        public async Task<Sondages> GetSondagesByCloseToken(string closeToken)
        {
            if (string.IsNullOrEmpty(closeToken))
            {
                throw new ArgumentNullException(nameof(closeToken));
            }
            Sondages sondage = await _context.Sondages.Include(c => c.Accounts).FirstOrDefaultAsync(c => c.closeToken == closeToken);
            return sondage;
        }
        public async Task<Sondages> GetSondagesByPublicToken(string publicToken)
        {
            if (string.IsNullOrEmpty(publicToken))
            {
                throw new ArgumentNullException(nameof(publicToken));
            }
            Sondages sondage = await _context.Sondages
                .Include(c => c.Accounts)
                .FirstOrDefaultAsync(c => c.publicToken == publicToken);
            return sondage;
        }
        public async Task CreateNew(Sondages sondage)
        {
            if(sondage == null)
            {
                throw new NullReferenceException(nameof(sondage));
            }
            _context.Sondages.Add(sondage);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateSondage(Sondages sondage)
        {
            if (sondage == null)
            {
                throw new NullReferenceException(nameof(sondage));
            }
            _context.Sondages.Update(sondage);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteSondage(Sondages sondage)
        {
            if (sondage == null)
            {
                throw new NullReferenceException(nameof(sondage));
            }
            _context.Sondages.Remove(sondage);
            await _context.SaveChangesAsync();
        }
    }
}
