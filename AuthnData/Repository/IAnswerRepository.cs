﻿using AuthnData.Classes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuthnData.Repository
{
    public interface IAnswerRepository
    {
        Task<List<Answers>> GetAllAsync();
        Task<List<Answers>> GetAllBySondageId(int SondageId);
        Task Create(Answers answer);
        Task Update(Answers answer);
        Task Delete(Answers answer);
    }
}
