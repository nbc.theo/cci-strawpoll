﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuthnData.Models.Sondage
{
    public class AddSondageViewModel
    {
        [Required]
        [Display(Name = "Question")]
        public string question { get; set; }
        [Display(Name = "Réponse")]
        public List<string> answer { get; set; }
        [Display(Name = "Date d'expiration du Sondage")]
        public DateTime expirationDate { get; set; }
        [Required]
        [Display(Name = "Question à choix multiple ?")]
        public bool isMultipleAnswer { get; set; }
        [Required,Display(Name ="Expire après une date ?")]
        public bool expireWithDate { get; set; }
        [Required, Display(Name = "Je souhaite être notifié par mail lorsque quelqu'un réponds à mon sondage")]
        public bool notifiedByMail { get; set; }
    }
}
