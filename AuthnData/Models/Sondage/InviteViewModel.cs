﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Models.Sondage
{
    public class InviteViewModel
    {
        [Required]
        [Display(Name = "Mail que vous invitez à participer au sondage")]
        public List<string> emailInvited { get; set; }
    }
}
