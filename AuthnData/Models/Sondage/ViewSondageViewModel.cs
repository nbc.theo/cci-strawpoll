﻿using System.Collections.Generic;

namespace AuthnData.Models.Sondage
{
    public class ViewSondageViewModel
    {
        public int sondageId { get; set; }
        public List<string> answerUser { get; set; }
    }
}
