﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Models.Login
{
    public class ChangePassViewModel
    {
        [Required(ErrorMessage = "Entrez votre Mot de passe actuel."), Display(Name = "Mot de passe actuel"), DataType(DataType.Password)]
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "Entrez un nouveau Mot de passe.")]
        [DataType(DataType.Password)]
        [Display(Name = "Nouveau Mot de Passe")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Entrez la confirmation du nouveau Mot de passe.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmer le nouveau Mot de Passe")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
