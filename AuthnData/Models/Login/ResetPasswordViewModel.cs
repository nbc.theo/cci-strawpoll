﻿namespace AuthnData.Models.Sondage
{
    public class ResetPasswordViewModel
    {
        public string Email { get; set; }
    }
}
