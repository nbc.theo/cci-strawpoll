﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Models.Login
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }
        [Required,Display(Name = "Se souvenir de moi")]
        public bool RememberMe { get; set; }
    }
}
