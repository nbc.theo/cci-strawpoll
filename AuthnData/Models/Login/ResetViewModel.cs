﻿using System.ComponentModel.DataAnnotations;

namespace AuthnData.Models.Login
{
    public class ResetViewModel
    {
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage ="Les Mots de passe ne correspondent pas")]
        public string ConfirmPassword { get; set; }
    }
}
