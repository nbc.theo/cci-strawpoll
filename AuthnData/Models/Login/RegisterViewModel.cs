﻿using System.ComponentModel.DataAnnotations;

namespace AuthnData.Models.Login
{
    public class RegisterViewModel
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Entrez une adresse e-mail.")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Entrez un Mot de passe.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de Passe")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Entrez la confirmation du Mot de passe.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmer le Mot de Passe")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Entrez un Nom d'Utilisateur.")]
        [Display(Name = "Nom d'Utilisateur")]
        [MaxLength(50, ErrorMessage = "Le Nom d'Utilisateur ne doit pas dépasser 50 charactères.")]
        public string Username { get; set; }
    }
}
