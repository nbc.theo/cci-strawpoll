﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Classes
{
    public class InvitedUsers
    {
        [Required]
        public string Mail { get; set; }
        [Required]
        public int SondageId { get; set; }
    }
}
