﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuthnData.Classes
{
    public class Sondages
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public Accounts Accounts { get; set; }
        [Required]
        public byte isMultipleAnswer { get; set; }
        [Required]
        public string viewToken { get; set; }
        [Required]
        public string closeToken { get; set; }
        [Required]
        public string publicToken { get; set; }
        [Required]
        public DateTime expirationDate { get; set; }
        [Required]
        public byte expireWithDate { get; set; }
        [Required]
        public byte isClosed { get; set; }
        [Required]
        public byte notifiedByMail { get; set; }
    }
}
