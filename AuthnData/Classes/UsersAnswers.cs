﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthnData.Classes
{
    public class UsersAnswers
    {
        [Required]
        public int accountId { get; set; }
        [Required]
        public int sondageId { get; set; }
        [Required]
        public string answers { get; set; }
        [Required]
        public DateTime answerDate { get; set; }
    }
}
