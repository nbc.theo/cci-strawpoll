﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AuthnData.Classes
{
    public class Answers
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Content { get; set; }

        public Sondages Sondages { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int ordreNumber { get; set; }
    }
}
