﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AuthnData.Classes
{
    public class Accounts
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime LastConnectionDate { get; set; }
        public DateTime LastResetPasswordDate { get; set; }
        public string ResetPasswordToken { get; set; }
        public byte RememberMe { get; set; }
        public byte isConfirmed { get; set; }
        public string ConfirmationToken { get; set; }
    }
}
