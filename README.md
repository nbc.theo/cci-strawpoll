Créer un fichier de stratégie pour approuver le certificat HTTPs avec Firefox

Créer un fichier de stratégie à l’adresse :

    Windows : %PROGRAMFILES%\Mozilla Firefox\distribution\policies.json
    MacOS : Firefox.app/Contents/Resources/distribution
    Linux : consultez approuver le certificat avec Firefox sur Linux dans ce document.

Ajoutez le code JSON suivant au fichier de stratégie Firefox :
JSON

{
  "policies": {
    "Certificates": {
      "ImportEnterpriseRoots": true
    }
  }
}

le fichier de stratégie précédent permet à Firefox d’approuver les certificats des certificats approuvés dans le magasin de certificats Windows. La section suivante fournit une autre méthode pour créer le fichier de stratégie précédent à l’aide du navigateur Firefox.

Configurer l’approbation du certificat HTTPs à l’aide du navigateur Firefox

Définissez security.enterprise_roots.enabled = true à l’aide des instructions suivantes :

    Entrez about:config dans le navigateur Firefox.
    Sélectionnez accepter le risque et continuer si vous acceptez le risque.
    Sélectionner Afficher tout
    Définir security.enterprise_roots.enabled = true
    Quitter et redémarrer Firefox

Pour plus d’informations, consultez Configuration des autorités de certification (ca) dans Firefox et le fichier Mozilla/Policy-Templates/Readme.
